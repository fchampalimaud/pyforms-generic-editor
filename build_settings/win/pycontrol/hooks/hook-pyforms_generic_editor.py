from PyInstaller.utils.hooks import collect_submodules, collect_data_files

from confapp import conf

hiddenimports = [
'pyforms_generic_editor.settings',
'pyforms_generic_editor.resources', 
'pycontrolgui_plugin.settings',
'pycontrolgui_plugin.resources',
'pycontrolgui_plugin_timeline.settings', 
'session_log_plugin.settings'] \
+ collect_submodules('pyforms_generic_editor.models') \
+ collect_submodules('pycontrolgui_plugin.models') \
+ collect_submodules('pycontrolgui_plugin_timeline.models') \
+ collect_submodules('session_log_plugin.models')

datas = [
		('pyforms_generic_editor\\resources', 'pyforms_generic_editor\\resources'),
		] \
		+ collect_data_files('pyforms_generic_editor') \
		+ collect_data_files('pycontrolgui_plugin') \
		+ collect_data_files('pycontrolgui_plugin_timeline') \
		+ collect_data_files('session_log_plugin')

