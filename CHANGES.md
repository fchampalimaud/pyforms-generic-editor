## v1.4.6 (2017/06/23)
Bpod build files are now on bpod project
Looks for “user_settings.py” instead of “pyforms_generic_editor_user_settings.py” when importing on main script

## v1.4.5 (2017/06/22)
Fixes bug related to opening user settings
Looks for “user_settings.py” instead of “pyforms_generic_editor_user_settings.py”
Adds support for user settings when users run application directly from a library

## v.1.4.4 (2017/05/04)
Adds support for new pybpod default plugin (pybpod-gui-plugin-session-history)
Updates default user settings

## v.1.4.3 (2017/05/03)
Prevents app from closing if errors occur when saving project
Minor enhancements for build settings
py2app support for matplotlib
Include matplotlib as dependency for windows pyinstaller script

## v.1.4.2 (2017/04/28)
Adds option to enable/disable Matplotlib and PyQt5 Web for pyinstaller
Enhances how pyforms and controls are loaded (disable PyQt Web, OpenGL and visvis on windows by default)
Load user plugins from folder
Fixes "About window"
Adds support for handling erros on project opening
Raise error when project is open without a valid settings path
Adds support for closing project on background (without user prompt confirmation). This is useful for error handling.
Handle invalid user settings file without closing app
Handle invalid session files without closing app
When selecting tree node, catch unexpected errors from windows, preventing app kill

## v1.4.1.beta (2017/04/18)
Improves Mac OS build script
Fixes problem for editing user settings inside the app when in Mac OS bundle
Prompts for saving project changes when choosing quit option on menu
Adds support for editing user settings inside the app
Prompt for saving project changes on exit
Adds build scripts for pycontrol-gui-plugin

## v1.4.0.beta (2017/04/17)
Fixes plugins finder dev mode

## v.1.3.0 (2017/13/04)
Preparing Mac OS and Windows bundles.
Minor fixes.


## v.1.2.0 (2017/04/04)
Adds support for Qt5 (it should be still compatible with Qt4)
