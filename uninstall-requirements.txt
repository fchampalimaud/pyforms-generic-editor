# DO NOT CHANGE THIS FILE
# DUPLICATE AND SAVE IT AS uninstall-requirements-dev-YOURNAME.txt
# run like this: pip3 uninstall -r uninstall-requirements-dev-YOURNAME.txt -y

# 3rd party dependencies
pyserial
Send2Trash
python-dateutil
numpy
PyQt5
sip
qscintilla

# swp dependencies
pyforms
pyforms
logging-bootstrap
pybpod-api
pybranch


# plugins
pybpod-gui-plugin
pybpod-gui-plugin-timeline
session-log-plugin